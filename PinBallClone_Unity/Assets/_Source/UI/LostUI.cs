using System;
using LevelParts;
using LevelParts.Ball;
using UnityEngine;

namespace UI
{
    public class LostUI : MonoBehaviour
    {
        [SerializeField] private GameObject lostPanel;

        private void Awake()
        {
            lostPanel.SetActive(false);
        }

        public void Init(BallsManager ballsManager)
        {
            ballsManager.OnOutOfBalls += ShowLostPanel;
        }

        private void ShowLostPanel()
        {
            lostPanel.SetActive(true);
        }
    }
}