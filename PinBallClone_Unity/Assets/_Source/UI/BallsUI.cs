using System;
using System.Collections.Generic;
using LevelParts;
using UnityEngine;

namespace UI
{
    public class BallsUI : MonoBehaviour
    {
        [SerializeField] private GameObject ballImagePrefab;
        [SerializeField] private Transform ballImagesPanel;
        
        private int _ballCount;
        private List<GameObject> _ballImages = new List<GameObject>();

        public void Init(int ballCount, DeadZone deadZone)
        {
            _ballCount = ballCount;

            for (int i = 0; i < _ballCount; i++)
            {
                GameObject newBallImage = Instantiate(ballImagePrefab, ballImagesPanel);
                _ballImages.Add(newBallImage);
            }
            
            deadZone.OnKill += SubtractBall;
        }


        private void SubtractBall()
        {
            _ballCount--;
            _ballImages[_ballCount].SetActive(false);
        }
    }
}