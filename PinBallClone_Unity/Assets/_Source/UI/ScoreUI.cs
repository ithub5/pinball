using LevelParts;
using LevelParts.Ball;
using TMPro;
using UnityEngine;

namespace UI
{
    public class ScoreUI : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI scoreText;

        private BallsManager _ballsManager;

        public void Init(BallsManager ballsManager)
        {
            _ballsManager = ballsManager;
            _ballsManager.OnScoreUpdate += UpdateScore;
        }
        
        private void UpdateScore()
        {
            scoreText.text = _ballsManager.Score.ToString();
        }
    }
}