namespace InputSystem
{
    public class InputHandler
    {
        private readonly MainActions _actions;

        public MainActions.UseActions UseActions => _actions.Use;
        
        public InputHandler()
        {
            _actions = new MainActions();
            _actions.Enable();
        }
    }
}