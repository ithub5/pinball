using InputSystem;
using LevelParts;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

namespace Core
{
    public class Restarter
    {
        private readonly InputHandler _input;

        public Restarter(InputHandler input)
        {
            _input = input;
            
            _input.UseActions.Restart.performed += RestartLevel;
        }

        private void RestartLevel(InputAction.CallbackContext ctx) => SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}