using InputSystem;
using LevelParts;
using LevelParts.Ball;
using LevelParts.Flippers;
using UI;
using UnityEngine;

namespace Core
{
    public class Bootstrapper : MonoBehaviour
    {
        [SerializeField] private Launcher launcher;

        [SerializeField] private Flipper leftFlipper;
        [SerializeField] private Flipper rightFlipper;

        [SerializeField] private GameObject ballPrefab;
        [SerializeField] private Transform ballSpawnPos;
        [SerializeField] private int ballsCount;
        
        [SerializeField] private DeadZone deadZone;
        [SerializeField] private BallsUI ballsUI;
        [SerializeField] private ScoreUI scoreUI;
        [SerializeField] private LostUI lostUI;

        private void Awake()
        {
            InputHandler input = new InputHandler();
            launcher.Init(input);

            new FlipperMaster(leftFlipper, rightFlipper, input);

            BallsManager ballsManager = new BallsManager(ballPrefab, ballSpawnPos, ballsCount, deadZone);
            ballsManager.Init();
            
            ballsUI.Init(ballsCount, deadZone);
            scoreUI.Init(ballsManager);
            lostUI.Init(ballsManager);

            new Restarter(input);
        }
    }
}
