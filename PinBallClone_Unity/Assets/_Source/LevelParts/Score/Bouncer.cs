using UnityEngine;
using UnityEngine.Serialization;

namespace LevelParts.Score
{
    public class Bouncer : MonoBehaviour, IScore
    {
        [SerializeField] private float bonceMultiplier;
        [field: SerializeField] public int Score { get; private set; }
        
        private void OnCollisionEnter(Collision collision)
        {
            if (collision.rigidbody == null)
                return;
            
            collision.rigidbody.AddForce(collision.impulse.normalized * (-1 * bonceMultiplier), ForceMode.Impulse);
        }
    }
}