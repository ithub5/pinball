namespace LevelParts.Score
{
    public interface IScore
    {
        public int Score { get; }
    }
}