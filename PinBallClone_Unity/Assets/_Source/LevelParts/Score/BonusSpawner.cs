using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LevelParts.Score
{
    public class BonusSpawner : MonoBehaviour
    {
        [SerializeField] private Transform[] spawnPos;
        [SerializeField] private GameObject[] bonusPrefabs;

        [SerializeField] private float minSpawnDelay;
        [SerializeField] private float maxSpawnDelay;

        private List<Transform> availablePos;

        private void Awake()
        {
            availablePos = new List<Transform>(spawnPos);

            StartCoroutine(Spawn());
        }

        private IEnumerator Spawn()
        {
            while (true)
            {
                yield return new WaitForSeconds(Random.Range(minSpawnDelay, maxSpawnDelay));

                if (availablePos.Count <= 0)
                    continue;
                
                int randomPosIndex = Random.Range(0, availablePos.Count);
                int randomPrefabIndex = Random.Range(0, bonusPrefabs.Length);

                Bonus bonus = Instantiate(bonusPrefabs[randomPrefabIndex], availablePos[randomPosIndex]).GetComponent<Bonus>();

                Transform removedPos = availablePos[randomPosIndex];
                bonus.OnCollected += () => availablePos.Add(removedPos);
                
                availablePos.RemoveAt(randomPosIndex);
            }
        }
    }
}