using System;
using UnityEngine;

namespace LevelParts.Score
{
    public class Bonus : MonoBehaviour, IScore
    {
        [field: SerializeField] public int Score { get; private set; }

        public event Action OnCollected;

        public void Collect()
        {
            gameObject.SetActive(false);
            OnCollected?.Invoke();
        }
    }
}