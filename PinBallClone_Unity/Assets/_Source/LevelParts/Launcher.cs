using System;
using System.Collections;
using InputSystem;
using UnityEngine;
using UnityEngine.InputSystem;

namespace LevelParts
{
    public class Launcher : MonoBehaviour
    {
        [SerializeField] private float oneStepStackForce;
        [SerializeField] private float waitBeforeStackForceDelay;
        [SerializeField] private float maxStackForce;
        
        private InputHandler _input;

        private Rigidbody _target;
        private float _launchForceAmount;

        public void Init(InputHandler input)
        {
            _input = input;
            
            _input.UseActions.Launch.started += StartLaunch;
            _input.UseActions.Launch.canceled += ReleaseLaunch;
        }

        private void OnTriggerEnter(Collider other) => _target = other.attachedRigidbody;

        private void OnTriggerExit(Collider other) => _target = null;

        private void StartLaunch(InputAction.CallbackContext ctx)
        {
            StartCoroutine(CountForce());
        }

        private IEnumerator CountForce()
        {
            _launchForceAmount = 0f;
            while (true)
            {
                _launchForceAmount += oneStepStackForce;
                if (_launchForceAmount > maxStackForce)
                {
                    _launchForceAmount = maxStackForce;
                    yield break;
                }
                yield return new WaitForSeconds(waitBeforeStackForceDelay);
            }
        }

        private void ReleaseLaunch(InputAction.CallbackContext ctx)
        {
            StopCoroutine(CountForce());
            if (_target == null)
                return;
            
            _target.AddForce(transform.forward * _launchForceAmount, ForceMode.Impulse);
        }
    }
}