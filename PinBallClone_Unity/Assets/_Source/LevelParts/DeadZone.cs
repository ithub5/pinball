using System;
using UnityEngine;

namespace LevelParts
{
    public class DeadZone : MonoBehaviour
    {
        public event Action OnKill;
        
        private void OnTriggerEnter(Collider other)
        {
            other.gameObject.SetActive(false);
            OnKill?.Invoke();
        }
    }
}