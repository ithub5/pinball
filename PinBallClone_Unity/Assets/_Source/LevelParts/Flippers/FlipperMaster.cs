using InputSystem;
using UnityEngine.InputSystem;

namespace LevelParts.Flippers
{
    public class FlipperMaster
    {
        private Flipper _leftFlipper;
        private Flipper _rightFlipper;

        public FlipperMaster(Flipper leftFlipper, Flipper rightFlipper, InputHandler input)
        {
            _leftFlipper = leftFlipper;
            _rightFlipper = rightFlipper;

            input.UseActions.LeftFlipper.performed += UseLeftFlipper;
            input.UseActions.RightFlipper.performed += UseRightFlipper;
        }

        private void UseLeftFlipper(InputAction.CallbackContext ctx) => _leftFlipper.AddTorque();

        private void UseRightFlipper(InputAction.CallbackContext ctx) => _rightFlipper.AddTorque();
    }
}