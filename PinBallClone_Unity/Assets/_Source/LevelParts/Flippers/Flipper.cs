using UnityEngine;

namespace LevelParts.Flippers
{
    public class Flipper : MonoBehaviour
    {
        [SerializeField] private Rigidbody rb;
        [SerializeField] private float torqueForce;

        public void AddTorque() =>
            rb.AddTorque(transform.up * torqueForce, ForceMode.VelocityChange);
    }
}