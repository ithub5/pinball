using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

namespace LevelParts.Ball
{
    public class BallsManager
    {
        private GameObject _ballPrefab;
        private Transform _spawnPos;
        private int _ballsCount;
        private readonly DeadZone _deadZone;

        private Queue<GameObject> _balls = new Queue<GameObject>();
        
        public LevelParts.Ball.Ball CurrentBall { get; private set; }
        
        public event Action OnScoreUpdate;
        public int Score { get; private set; }

        public event Action OnOutOfBalls;

        public BallsManager(GameObject ballPrefab, Transform spawnPos, int ballsCount, DeadZone deadZone)
        {
            _ballPrefab = ballPrefab;
            _spawnPos = spawnPos;
            _ballsCount = ballsCount;
            _deadZone = deadZone;
        }

        public void Init()
        {
            for (int i = 0; i < _ballsCount; i++)
            {
                GameObject newBall = Object.Instantiate(_ballPrefab, _spawnPos);
                newBall.SetActive(false);
                _balls.Enqueue(newBall);
            }
            
            _deadZone.OnKill += GetBall;
            
            GetBall();
        }

        private void GetBall()
        {
            if (_balls.Count <= 0)
            {
                OnOutOfBalls?.Invoke();
                return;
            }
            GameObject currentBall = _balls.Dequeue();
            currentBall.SetActive(true);

            if (CurrentBall != null)
            {
                CurrentBall.OnScoreUpdate -= ScoreUp;
            }
            CurrentBall = currentBall.GetComponent<LevelParts.Ball.Ball>();
            CurrentBall.OnScoreUpdate += ScoreUp;
        }

        private void ScoreUp(int scoreToAdd)
        {
            Score += scoreToAdd;
            OnScoreUpdate?.Invoke();
        }
    }
}