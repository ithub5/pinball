using System;
using LevelParts.Score;
using UnityEngine;

namespace LevelParts.Ball
{
    public class Ball : MonoBehaviour
    {
        public event Action<int> OnScoreUpdate;

        private void OnCollisionEnter(Collision collision)
        {
            if (!collision.gameObject.TryGetComponent(out IScore scoringObject))
                return;
            
            OnScoreUpdate?.Invoke(scoringObject.Score);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (!other.TryGetComponent(out Bonus scoringObject))
                return;

            scoringObject.Collect();
            OnScoreUpdate?.Invoke(scoringObject.Score);
        }
    }
}